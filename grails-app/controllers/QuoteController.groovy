import qotd.Quote

/**
 * Created by GRamis on 30.06.2015.
 */
class QuoteController {

    def scaffold = Quote;

    def quoteService

    def index = {
    }

    def home = {
        render "<h1>Real Programmers do not eat Quiche</h1>"
    }

    def random = {
        def randomQuote = quoteService.getRandomQuote()
        [quote: randomQuote]
    }

    def ajaxRandom = {
        def randomQuote = quoteService.getRandomQuote()
        render "<q>${randomQuote.content}</q>" +
                "<p>${randomQuote.author}</p>"
    }
}