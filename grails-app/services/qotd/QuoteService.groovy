package qotd

import grails.transaction.Transactional

@Transactional
class QuoteService {

    def serviceMethod() {

    }

    def getStaticQuote(){
        return new Quote(author: "Anon",
        content: "Real Programmers")
    }

    def getRandomQuote(){
        def allQuotes = Quote.list()
        def randomQuote = null
        if (allQuotes.size() > 0){
            def randomIdx = new Random().nextInt(allQuotes.size())
            randomQuote = allQuotes[randomIdx]
        } else {
            randomQuote = getStaticQuote()
        }
        return randomQuote
    }
}
