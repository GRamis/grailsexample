<%--
  Created by IntelliJ IDEA.
  User: GRamis
  Date: 30.06.2015
  Time: 20:36
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Random Quote</title>
    <g:javascript library="prototype"/>
</head>

<body>
    <ul id="menu">
        <li>
            <g:remoteLink action="ajaxRandom" update="quote">
                Next Quote
            </g:remoteLink>
        </li>
        <li>
            <g:link action="list">
                Admin
            </g:link>
        </li>
    </ul>
    <div id="quote">
        <q>${quote.content}</q>
        <q>${quote.author}</q>
    </div>

</body>
</html>